## Project Setup

```sh
docker-compose up -d --build
```

Run migrations in container:
```sh
php bin/console doctrine:migrations:migrate
```
# todo list
- async jobs
- extended validation
- password hashing
- password change confirming
- auto-migration
- load env
- swagger
