<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    #[Route('/users-page', name: 'users_page')]
    public function index(): Response
    {
        return $this->render('pages/users.html.twig', [
            'user_first_name' => '$userFirstName',
            'notifications' => '$userNotifications',
        ]);
    }
}
