<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Request\UserCreateRequest;
use App\Request\UserUpdateRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{

    #[Route('/users', name: 'get_users', methods: ['GET'])]
    public function get(Request $request, SerializerInterface $serializer, UserRepository $userRepository): JsonResponse
    {
        $models = $userRepository->findBy($request->query->all());
        $data = $serializer->serialize($models, JsonEncoder::FORMAT);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    #[Route('/users', name: 'create_user', methods: ['POST'])]
    public function create(UserCreateRequest $request, UserRepository $userRepository): JsonResponse
    {
        $userRepository->createUser($request->toDTO());

        return $this->json([
            'message' => 'User created',
        ]);
    }

    #[Route('/users/{id}', name: 'get_user', methods: ['GET'])]
    public function singleUser(int $id, UserRepository $userRepository, SerializerInterface $serializer): JsonResponse
    {
        $model = $userRepository->find($id);
        $data = $serializer->serialize($model, JsonEncoder::FORMAT);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    #[Route('/users/{id}', name: 'update_user', methods: ['POST'])]
    public function updateUser(int $id, UserUpdateRequest $request, UserRepository $userRepository): JsonResponse
    {
        $model = $userRepository->find($id);
        $userRepository->updateUser($model, $request->toDTO());

        return $this->json([
            'message' => 'User updated',
        ]);
    }
}
