<?php

namespace App\Request;

use App\DTO\UserCreateDto;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

class UserCreateRequest extends BaseRequest implements ToDtoInterface
{

    #[NotNull]
    #[Type('string')]
    protected $user_name;

    #[NotNull]
    #[Type('string')]
    #[Email]
    protected $email;

    #[NotNull]
    #[Type('string')]
    public $password;

    #[NotNull]
    #[EqualTo(
        propertyPath: "password"
    )]
    protected $password_confirm;

    #[Pure]
    public function toDTO(): UserCreateDto
    {
        return new UserCreateDto(
            $this->email,
            $this->user_name,
            $this->password,
        );
    }
}
