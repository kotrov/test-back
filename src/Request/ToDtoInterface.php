<?php

namespace App\Request;

use App\DTO\DtoInterface;

interface ToDtoInterface
{
    public function toDTO(): DtoInterface;
}
