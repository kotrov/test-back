<?php

namespace App\Request;

use App\DTO\UserUpdateDto;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

class UserUpdateRequest extends BaseRequest implements ToDtoInterface
{

    #[NotNull]
    #[Type('string')]
    protected $user_name;

    #[NotNull]
    #[Type('string')]
    #[Email]
    protected $email;

    #[Pure]
    public function toDTO(): UserUpdateDto
    {
        return new UserUpdateDto(
            $this->email,
            $this->user_name,
        );
    }
}
