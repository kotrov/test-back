<?php

namespace App\DTO;

class UserCreateDto implements DtoInterface
{
    public function __construct(
        private string $email,
        private string $user_name,
        private string $password,
    )
    {

    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getUserName(): string
    {
        return $this->user_name;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
