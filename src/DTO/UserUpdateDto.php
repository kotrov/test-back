<?php

namespace App\DTO;

class UserUpdateDto implements DtoInterface
{
    public function __construct(
        private string $email,
        private string $user_name,
    )
    {

    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getUserName(): string
    {
        return $this->user_name;
    }
}
